---
title: "Formation R débutant"
author: "Jimmy Lopez"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output: html_document
---

-------------------------

## 0) Les variables et l'affectation

```{r, background="green"}
x <- 1
x
```

```{r}
y = 1
y
```

```{r}
x <- y <- z <- 23
```

Remarque : porté des variables ! différents des autres languages


```{r}
VAR <<- 42
```

-------------------------

## 1) Les types de base

### 1.1) numeric

```{r}
a <- 3.14
b <- 23
class(a)
```

### 1.2) character

```{r}
fname <- "Jimmy"; lname <- "Lopez" 
name <- paste(fname, lname, sep = " ") 
name
```

```{r}
s1 <- paste0("Eco", "tron")
s1
```
### 1.3) logical
```{r}
u <- TRUE; v <- FALSE
u2 <- T; v2 <- F
u
```

### 1.4) other 

```{r}
p1 <- NULL
p2 <- NA
p3 <- NaN
p1
p2
p3
```

-------------------------

## 2) Les autres structures des données

### 2.1) vector

Un seul type mais :  TRUE < 3 < "s" et NA NULL NaN neutre

```{r}
v1 <- c(2, 3, 5) 
v1
v2 <- c(1:10)
v2
v3 <- c(rep(1, 10))
v3
v4 <- c(a, b)
v4
v5 <- c(TRUE, FALSE)
v5
v6 <- c(v1, v5) # ! convertion de type
v6
v7 <- c("toto", "tutu")
v7
v8 <- c(v6, v7) # ! convertion de type
v8
v9 <- c(c(3,4), "3")
v9
```

**! l'indice des tableaux ou autre commence à 1**

```{r}
v1 <- c(2, 3, 5) 
v1[1]
v1[2] <- 42
v1
v1 <- c(v1, 13)
v1
v1[-4] # ! supprime l'indice 4
v1
v1[1:2]
```

Pour avoir la taille de notre vecteur

```{r}
v1 <- c(2, 3, 5) 
length(v1)
```

### 2.2) matrix

Un seul type de donnée

```{r}
m1 <- matrix(0, nrow = 5, ncol = 4)
m1
m2 <- matrix(1:20, nrow = 5, ncol = 4)
m2

length(m1)
nrow(m1)
ncol(m1)

m2[3,2]
m2[3,]
m2[,2]
m2[c(2,4), c(1,3)]
```

Translation

```{r}
m2 <- matrix(1:20, nrow = 5, ncol = 4)
m2
t(m2)
```

Diagonale

```{r}
m2 <- matrix(1:16, nrow = 4, ncol = 4)
diag(m2)
```

### 2.3) array

Un seul type de donnée

```{r}
ar1 <- array(1:60, dim=c(3,4,5))
ar1
```

### 2.4) list

Plusieurs type de données

```{r}
l1 <- list(e1=c(2, 3, 5), e2=c("aa", "bb"), e3=3)
l1

l1$e1 
l1["e1"]
l1[["e1"]]
```

```{r}
l1$e3 = c(FALSE)
#pas obligé de nommer
l2 <- list(1,2,3)
l2
length(l2) # la taille de notre liste
```

Attention : ne permet pas de fusionner
```{r}
l3 <- list(l1, l2) # list of list
l3
```

Pour la fusion de list
```{r}
l4 <- append(l1, l2) # l2 dans l1
l4 
```

### 2.5) data.frame

```{r}
prenom <- c("jimmy", "khalid", "joana")
nom <- c("lopez", "belkhir", "sauze")
team <- c("MBB", "MBB", "ecotron")

data <- data.frame(prenom, nom, team)
data

rownames(data) <- c("p1", "p2", "p3")
colnames(data) <- c("nom", "prenom", "plateforme") 
data

data["p1","nom"] 
data["nom"]
data["p1",]
data[c("nom", "plateforme")] 
data[1,2]
data$prenom

data$new_c <- c(4,3,2)
data

data[3, 2] # factor 

nrow(data)
ncol(data)
```

Fusion de data.frame : doit avoir même nom de colonne 
```{r}
fred <- data.frame(nom="fred",prenom="snow",plateforme="NEW") 
bob <- data.frame(nom="bob",prenom="daul",plateforme="NEW2") 
data <- rbind(fred, bob)
data
```

Les noms des colonnes seront choisie pour vous
```{r}
d1 <- data.frame(1,2,3)
d1
```

## 3) Les operateurs arithmétiques

* \+ : addition
* \- : Soustraction
* \* : multiplication
* \/ : division
* \^ : exposant
* x %% y : modulo 
* x %/% y :	Partie entière de la division 
* x %*% x : multiplication de matrice/vecteur

```{r}
o1 <- 3 + 2.5
o1
o2 <- TRUE + 3
o2
o3 <- c(1:10) * 3
o3
```

! Possible mais la taille d'un objet plus long n'est pas multiple de la taille d'un objet plus court
```{r}
o4 <- c(1:10) * c(2, 3, 5)  #  v2 * c(2,3,4)
o4
o5 <- m2 * 2
o5
o6 <- m2 * v2 # mais la aussi attention à la taille r * c * n = l 
o6
```
```{r}
5 / 2 
5 %% 2 
5 %/% 2 
2 ^ 3
```
## 4) Les opérateurs logiques & les structures conditionelles

* \< : inférieur à
* \<= :	inférieur ou égal à
* \> 	: supérieur
* \>= :	supérieur ou égal à
* \==  : exactement égale à
* \!=  : différent de
* \x|y : x ou y
* \x & y : x et ou
* isTRUE(x) 
* \! : négation

* **En R pas de \++ \-- \+= \-=**

```{r}
x <- 6

if( x == 3 ) {
  print("OK")
} else if ( x < 5 ) {
  print (" < 5 ")
} else if (  x > 7 ) {
  print (" > 7 ")
} else {
  print("else")
}
```

## 5) Les boucles

For par élément
```{r}
v2 <- c(1:10)
for (e in v2) {
  print(e)
}
```

For par indice
```{r}
v2 <- c(1:10)
for(x in 1:length(v2)) {
  print(v2[x])
}
```

Next et Break
```{r}
v2 <- c(1:10)
for (e in v2) {
  
  if (!(e %% 2)){ # 0 = FALSE ; other numeric = TRUE
    next
  }
  
  if(e == 9) {
    break
  }

  print(e)
}
```

```{r}
m3 <- matrix(0, 4, 5)
m3
for (i in 1:nrow(m3)) {
  for (j in 1:ncol(m3)) {
    m3[i, j] <- i + j
  }
}
m3
```

While
```{r}
i <- 1
while (i < 6) {
  print(i)
  i = i+1
}
```
Repeat
```{r}
x <- 1
repeat {
  print(x)
  x = x+1
  if (x == 6){
    break
  }
}
```

## 6) Les fonctions

```{r}
increment <- function(x) {
  
  res <- x + 1

  return(res)
}

increment(22)

```

```{r}
mult <- function(x = 1, y = 1) {

  return(x*y)

}

mult()
mult(3)
mult(y = 2) 
mult(3,2)
mult(y <- 2) # attention car init la var y et en plus x = 2 et y = 1

```

```{r}
getPI <- function() {
  return(3.14159265359)
}

getPI()

```

Quelques fonction de base :

```{r}
x <- c(9,5,6,8,4,11,36,59,12)

sum(x) 	# somme des éléments de x
prod(x) #	produit des éléments de x
max(x) 	# maximum des éléments de x
min(x) 	# minimum des éléments de x
which.max(x) 	# retourne l'indice du maximum des éléments de x
which.min(x) 	# retourne l'indice du minimum des éléments de x
range(x) 	# idem que c(min(x), max(x)); vector
length(x) 	# nombre d'éléments dans x
mean(x) 	# moyenne des éléments de x
median(x) 	# médiane des éléments de x

class(x)
as.numeric("56") # else NA
as.character(123) # as. ...

is.null(x) #is. ...
is.numeric(x)
is.vector(x)

```

Attention ne pas nommer des fonctions de base ou de package

```{r}
sum <- function(x) {
  return(42)
}

x <- c(9,5,6,8,4,11,36,59,12)

sum(x)
base::sum(x)
```

Fonction dans une fonction
```{r}
foo <- function() { # function in function

  bar <- function() { # private function, possible mais peut utile
    return(4)
  }
  
  return(3+bar())
}

foo()
```


## 7) Comment optimiser les boucles

Attention si dépendances entre les indices 

```{r}
f1 <- function(x) {
  return(x+1)
}

x <- c(9,5,6,8,4,11,36,59,12)

sapply(x, f1) # sapply return vector, lapply list of element
```

```{r}
m <- matrix(data=cbind(1:6, 5:10, 10:15), nrow=6, ncol=3)
m

apply(m, 1, mean) # 1 egale ligne 
apply(m, 2, mean) # 2 colonne ou c(1,2)
apply(m, c(1,2), mean) # 1 egale ligne 2 colonne ou c(1,2)
mean(m)
sapply(m, f1) # lapply return list of each function return
lapply(m, f1)
```

## 8) Lire / Ecrire un fichier csv

The data was extracted from the 1974 Motor Trend US magazine, and comprises fuel consumption and 10 aspects of automobile design and performance for 32 automobiles (1973–74 models). 
```{r}
mtcars 

write.csv(mtcars, "exemple_1.csv")
write.csv(mtcars, "exemple_2.csv", row.names = F)
write.csv2(mtcars, "exemple_3.csv")

cars <- read.csv("exemple_1.csv")
cars
cars3 <- read.csv("exemple_3.csv", sep = ";")
cars3

# other write si pas un csv
```

## 9) Charger et utiliser un autre script R

```{r}
source("./tools.R") # on charge un autre script R

jimmy_sub1()
```

## 10) Utiliser un package R

```{r}
#install.packages('ggplot2')
library(ggplot2) 

data <- data.frame(a=1:7, b=c(2, 5, 6, 12, 20, 3, 7))
p <- ggplot(data=data, aes(x=a, y=b,colour="Cond")) 
p <- p + geom_point(size=4)
p <- p + geom_line(size=2) 
p

?ggplot2 # info sur le package 
?ggplot # info sur la fonction
```

## 11) La manipulation des données avec dplyr

```{r}
library (dplyr)

mtcars

select(mtcars, drat) # on sélectionne une colonne

mtcars["drat"] # équivalent classique

select(mtcars, -drat) # on élimine une colonne

mtcars[,! names(data) %in% c("drat")]  # équivalent classique

filter(mtcars, disp > 160, drat < 3) # pour filtrer 

f1 <- mtcars[mtcars$disp>160,]
f1[f1$drat < 3, ] # équivalent classique

arrange(mtcars, disp) # on range dans l'ordre croissant

mtcars[order(mtcars$disp),]  # équivalent classique

arrange(mtcars, desc(disp)) # on range dans l'ordre croissant

mtcars[order(-mtcars$disp),] # équivalent classique
```
Utilisation de l'operateur '%>%' pour plus de souplesse 

```{r}
mtcars %>% select(drat, wt, am) %>% filter(drat < 3) %>% arrange(desc(wt)) 

f1 <- mtcars[c("drat", "wt", "am")]
f2 <- f1[f1$drat < 3,]
f3 <- f2[order(-f2$wt),]
f3
```

## 12) Comment avoir une interactions avec une base de données

```{r}
library(RMySQL)
library(pool)

connectDB <- function() {
  pool <- dbPool(
    drv = RMySQL::MySQL(),
    dbname = "earthworms",
    host = "localhost",
    username = "root",
    password = "mbb"
  )
  return(pool)
}

#' Get the data in range from data base
#' @param name a character : the name of table in DB
#' @param start a character : the start Timestamp 
#' @param end a character : the end Timestamp 
#' @return a data.frame
get_data_in_range <- function(name, start, end) {
  pool = connectDB()
  sql <- paste0("select `Timestamp`, `Value` from `", name, "` WHERE `Timestamp` between '", start, "' AND '", end, "';")
  #print(sql)
  query <- suppressWarnings(sqlInterpolate(pool, sql))
  data <- suppressWarnings(dbGetQuery(pool, query))
  poolClose(pool) #important
  return(data)
}

# TAIR m1 consigne
data <- get_data_in_range("cfp_mac1_vir_cons_t_regul", "2018-03-03 07:19:38", "2018-03-03 18:36:14")

mean(data$Value)
max(data$Value)
min(data$Value)
max(data$Timestamp)
min(data$Timestamp)
```

## Questions ?

