Formation git
========================================================
author: Jimmy Lopez
date: 19 novembre, 2018
autosize: true

Sommaire
========================================================

+ Pourquoi utiliser Git

+ Guide niveau débutant 

+ Guide niveau intermédiaire

Introduction
========================================================

C'est quoi un VCS (version control system) ?
+ Garder des traces
+ Le travail à plusieurs
+ Sauvegarde et fiabilité

Introduction
========================================================
GIT = DVCS (Distributed version control system)

Toute copie de travail est autonome et contient tout l'historique. 

+ On peut créer une version sans la transmettre à un dépôt (offline)
+ On n'a plus de différence entre copie de travail et dépôt local
+ On peut échanger des versions avec plusieurs dépôts distants
+ On peut ANNULER des modifications
+ On peut faire des branches
+ On peut naviguer rapidement entre les versions
+ ...

Introduction
========================================================
Pourquoi GIT ?
+ Le plus populaire
+ Le plus fourni en documentation
+ Le plus fourni en outils
+ Le plus performant des DVCS.

Guide pour les débutants
========================================================

+ 1-n utilisateur(s)

+ 1 dépôt local + 1 dépôt en ligne (Gitlab mbb)

Débutant : Introduction technique
========================================================

Un dépôt : 

    + les fichiers du projet 
    + l'historique des modifications
    + la configuration du dépôt.

Dans un dépôt, les modifications sont matérialisées par des commits. 

Un commit :

    + un identifiant de commit (hash)
    + un auteur
    + un ensemble de modifications sur des fichiers (diffs)
    + un commit parent
    
    
Débutant : Installer Git
========================================================

Sous Linux : 
```{}
sudo apt-get install git-core
```

Sous Mac : 
```{}
https://git-scm.com/download/mac
```

Sous Window :
```{}
http://msysgit.github.io/
```

Débutant : Configurer Git
========================================================

Pour configurer son pseudo
```{}
git config --global user.name "jlopez"
```

Pour configurer son email
```{}
git config --global user.email jlopez@umontpellier.fr
```

Pour configurer l'éditeur de texte (pour les allergiques de vim)
```{}
git config --global core.editor gedit
```
```{}
git config --global core.editor "\"c:\Program Files\Notepad++\notepad++.exe\" %*"
```


Débutant : Gestion simple en local
========================================================

Pour initialisez un dépôt Git
```{}
git init
```

Pour avoir la documentation des commandes Git
```{}
git help
```

Pour avoir la documention et les options d'une commandes 
```{}
git help your_command
```

Débutant : Versionner des fichiers
========================================================

Pour versionner un fichier
```{}
git add my_file.txt
```

Pour versionner un dossier
```{}
git add /path/dir/
```

Pour créer une version

```{}
git commit -m "mon message"
```

Débutant : Visualiser l'historique
========================================================

Pour voir l'état courant du dépôt local
```{}
git status
```

Pour visualiser l'historique des commits
```{}
git log
```

Pour visualiser les modifications apportées
```{}
git diff
```

Débutant : Travailler avec des dépôts distants - Gitlab mbb
========================================================

Pour clonner un nouveau dépôt distant
```{}
git clone http://jlopez@gitlab.mbb.univ-montp2.fr/jlopez/mon_depot.git
```

Un nouveau dossier contenant le contenu du dépôts distant est créé en local.

Pour récuperer les modifications faite sur un dépôt distant
```{}
git pull origin master
```

Pour envoyer les modifications local sur un dépôt distant
```{}
git push origin master
```

Débutant : Pour ajouter un projet à un depôt distant existant
========================================================
```{}
cd existing_folder
git init
git remote add origin http://jlopez@gitlab.mbb.univ-montp2.fr/jlopez/mon_depot.git
git add .
git commit -m "Initial commit"
git pull origin master
git push -u origin master
```

Débutant : Revenir en arrière 
========================================================

Déversionner un fichier non commit
```{}
git add my_file
git reset my_file
```

Pour supprimer un commit non push
```{}
git add my_file
git commit -m "my modif"
git reset HEAD~1
```

Pour supprimer un commit push
```{}
(voir dans Intermédiaire)
```

Débutant : Gérer les conflits
========================================================

```{}
git pull origin master
Fusion automatique de test.txt 
CONFLIT (contenu) : Conflit de fusion dans test.txt
La fusion automatique a échoué ; réglez les conflits et validez le résultat.
```

<p style="text-align:center"><img src="conflit.png"  height="200" width="200"><p/>

Supprimer les conflits dans mon fichier (manuellement)

```{}
git add mon_fichier
git commit -m "Fix conflits"
git push origin master
```

Débutant : Ignorer des fichiers
========================================================

Pour ignorer un fichier il suffit de créer un fichier ".gitignore" à la racine du projet et de mettre les fichiers a ignorer dedans.
```{}
#contenu fichier .gitignore

#un fichier en particuler
mon_fichier.txt

#tout un dossier
/build

#tout un type de fichier
*.RData
```
Débutant : Procedure du débutant pour bien utiliser git
========================================================
```{}
git add mon_fichier
git commit -m "New modif"
git pull origin master
//Fix conflit si besoin
git push origin master
```

Guide pour les intermédiaires
========================================================

+ 1-n utilisateur(s)

+ 1 dépôt local + 1 dépôt en ligne (Gitlab mbb)

+ 1-n branches

+ Des astuces pour aller plus vite

Intermédiaire : Revenir en arrière 
========================================================
Pour supprimer un commits push
```{}
git reset --soft HEAD~1
ou
git reset --hard HEAD~1 (attention supprime en local le contenu)
...
git commit -m "new commit"
git push origin master -f (si pas les droits de forcer, voir sur gitlab)
```

Intermédiaire : Faire des modifications sur commit non push
========================================================

Si on a oublié un fichier après avoir commit (avant d'avoir push)
```{}
git add mon_fichier_oublié
git commit --amend --no-edit
```

Si on veut changer le message du dernier commit (avant d'avoir push)
```{}
git commit --amend -m "mon nouveau message"
```

Intermédiaire : Qu'est ce qu une branche ?
========================================================

Une branche est un historique de travail parallèle et indépendant du travail principal. 
<br/> <br/>
Une branche démarre à un point de divergence avec une autre dans l'arbre des commits.

Intermédiaire : Utilisation basique des branches
========================================================

Pour créer une nouvelle branche :
```{}
git branch name_branch
```

Pour se déplacer sur une branche :
```{}
git checkout name_branch
```

Pour fusionner avec la branche master et ma nouvelle branche : 

```{}
git checkout master
git merge name_branch
```

Intermédiaire : Les tags (étiquetes)
========================================================

Pour créer un tag : 
```{}
git tag -a v1.0 -m 'my version 1.0'
```

Lister les tags :
```{}
git tag
```

Pour créer un tag après coup :
```{}
git tag -a v0.5 -m 'version 0.5' 9fceb02
```

Remarques : quand on push les tags ne sont pas envoyé

Pour envoyer un tag :
```{}
git push origin v1.0
```
```{}
git push origin --tags
```



Conclusion
========================================================

http://rogerdudler.github.io/git-guide/index.fr.html<br/>
https://git-scm.com/


